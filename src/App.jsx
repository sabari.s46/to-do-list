import { Component } from "react";
import "./App.css";

export default class App extends Component {
  constructor(props) {
    super(props);

    // Setting up state
    this.state = {
      userInput: "",
      list: [],
    };
  }

  // Set a user input value
  updateInput(value) {
    this.setState({
      userInput: value,
    });
  }

  addItem() {
    if (this.state.userInput !== "") {
      const userInput = {
        // Add a random id which is used to delete
        id: Math.random(),

        // Add a user value to list
        value: this.state.userInput,
      };

      // Update list
      const list = [...this.state.list];
      list.push(userInput);

      // reset state
      this.setState({
        list,
        userInput: "",
      });
    }
  }

  deleteItem(key) {
    const list = [...this.state.list];

    // Filter values and leave value which we need to delete
    const updateList = list.filter((item) => item.id !== key);

    // Update list in state
    this.setState({
      list: updateList,
    });
  }

  editItem = (index) => {
    const todos = [...this.state.list];
    const editedTodo = prompt("Edit the todo:");
    if (editedTodo !== null && editedTodo.trim() !== "") {
      let updatedTodos = [...todos];
      updatedTodos[index].value = editedTodo;
      this.setState({
        list: updatedTodos,
      });
    }
  };

  render() {
    return (
      <div>
        <div>TODO LIST</div>
        <hr />
        <div>
          <input
            placeholder="Type here"
            value={this.state.userInput}
            onChange={(item) => this.updateInput(item.target.value)}
          />
          <button onClick={() => this.addItem()}>Add item</button>
        </div>

        <div>
          {/* map over and print items */}
          {this.state.list.map((item, index) => {
            return (
              <div key={index}>
                <div>
                  {item.value}
                  <span>
                    <button
                      onClick={() => {
                        this.deleteItem(item.id);
                        console.log(item);
                      }}
                    >
                      Delete
                    </button>
                    <button onClick={() => this.editItem(index)}>Edit</button>
                  </span>
                </div>
              </div>
            );
          })}
        </div>
      </div>
    );
  }
}
